# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 19:51:05 2018

@author: Juan
"""
import random
import matplotlib.pylab as plt
from scipy import stats 
import numpy as np
# se 1 cara y 0 sello
tupla = (0,1,2,3,4,5,6,7,8,9,10)
ensayos = 10
experimentos  = 10000
p = 0.40
ans = []
tmp = []
vectorExitos = []
count = 0
exito = 0

for i in range(experimentos):
    for j in range(ensayos):
        valor = 0
        r = random.random()
        if r <= p:
            valor = 1
        else:
            valor
        ans.append(valor)   
#print(ans)
for i in range(experimentos):
    tmp.append(ans[i])
    count = count + 1
    if count == 10:       
        exito = tmp.count(1)
        vectorExitos.append(exito)
        tmp.clear()
        count = 0
        exito = 0
#print(vectorExitos, '\n')
#print(len(vectorExitos))


vector  = []
for i in tupla:
    fmp = stats.binom.pmf(i, ensayos, p)
    vector.append(fmp)
    
print(vector)
#plt.hist(vectorExitos, bins = 100)
plt.hist(vector, bins = 100)
plt.plot(tupla, vector, '--')
plt.vlines(tupla, 0, vector, colors='b', lw=5, alpha=0.5)
plt.title('Función de Masa de Probabilidad')
plt.ylabel('probabilidad')
plt.xlabel('valores')
plt.show()