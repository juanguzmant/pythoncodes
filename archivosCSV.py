import csv
#variables globales
archivo = "archivo.csv"


#METODO 1
'''
doc = open(archivo,"w") # propiedades del archivo
doc_csv_w = csv.writer(doc) # se asigna el writer   
lista = [ ["Juan", 115], ["Jose",324], ["Julia", 324] ] # se crea una lista para ingresar datos al doc
#doc_csv_w.writerows(lista) # se ingresan los datos, FORMA 1 DE HACERLO

for x in lista:
    doc_csv_w.writerow(lista)
doc.close() # se cierra el archivo
'''
#METODO 2 (Lectura)
'''
with open(archivo, newline = '') as arhivo: # la sentencia with, autogestiona el cierre del archivo.
    reader = csv.reader(arhivo)
    for row in reader:
        print(row)
   
        '''

#Metodo 4 (Lectura)

doc = open(archivo, "r")
csvDocument = csv.reader(doc)
for (nombre, id) in csvDocument:
    print(nombre,id)
doc.close()
#METODO 3 (Escritura)
'''
with open(archivo,"w",newline = "") as archivoCSV:
    doc_csv_w = csv.writer(archivoCSV)
    lista = [["Juan","229"], ["Pedro", "435"]]
    doc_csv_w.writerows(lista)
'''
#METODO 4 (Control de datos)
'''
with open(archivo,"w") as archivoCSV:
    doc_csv_w = csv.writer(archivoCSV)
    lista = [[62],[64],[26],[565]]
  
    for dato in lista:
      if(dato[0] %2 == 0):
          doc_csv_w.writerow(dato)
      else:
          print("dato {} no se agrego".format(dato[0]))
'''


