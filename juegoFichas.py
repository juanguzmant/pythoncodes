#Juan Camilo Guzman Toro
#318957
''' Numero de fichas = 50
	20 grises 	(1)
	10 rojas	(5)
	8 verdes	(25)
	8 azules	(50)
	4 negras	(100) '''
from random import shuffle,randint
from matplotlib.pylab import hist,show
import matplotlib.pyplot as plt
import numpy as np
import statistics as stats

chipBag = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,5,5,5,5,5,5,5,5,5,
25,25,25,25,25,25,25,25,50,50,50,50,50,50,50,50,100,100,100,100]



fValues = []


number = int(input("numero de veces que desea sacar ficha: "))
for x in range(0,number):
	shuffle(chipBag)
	findedChip = chipBag.pop()
	fValues.append(findedChip)
	print(findedChip)
	#print(fValues)
	chipBag.append(findedChip)
	print(fValues)

fValues.sort()
'''print('la media es: ', stats.mean(fValues))
print('la mediana es :', stats.median(fValues))
print('la moda es: ', stats.mode(fValues))
print('la desviacion es: ',stats.stdev(fValues))
print('la varianza es: ',stats.variance(fValues))
print('el rango es: ',(max(fValues) - min(fValues)))'''
show(hist(fValues, bins = 'auto'))


print("Numero de Gris = ",numeroGris = valoresHallados.count(1))
print("Numero de Rojo = ",numeroRojo = valoresHallados.count(5))
print("Numero de Verde = ",numeroVerde = valoresHallados.count(25))
print("Numero de Azul = ",numeroAzul = valoresHallados.count(50))
print("Numero de Negro = ",numeroNegro = valoresHallados.count(100))

chipBag = [1,1,1,1,1,1,1,1,1,1,5,5,5,5,5,
25,25,25,25,50,50,50,50,100,100]

valoresHallados = []
numeroIntentos = int(input("Ingrese numero de intentos: "))

for i in range(numeroIntentos):
    fichaHallada = chipBag.pop(random.randrange(0,25,1))
    valoresHallados.append(fichaHallada)
    chipBag.append(fichaHallada)
    print("la ficha hallada es: ",fichaHallada)
    print(valoresHallados,"\n")


numeroGris = valoresHallados.count(1)
numeroRojo = valoresHallados.count(5)
numeroVerde = valoresHallados.count(25)
numeroAzul = valoresHallados.count(50)
numeroNegro = valoresHallados.count(100)
print("Numero de Gris = {}\n".format(numeroGris))
print("Numero de Rojo = {}\n".format(numeroRojo))
print("Numero de Verde = {}\n".format(numeroVerde))
print("Numero de Azul = {}\n".format(numeroAzul))
print("Numero de Negro = {}\n".format(numeroNegro))
valoresHallados.sort()
plt.hist(valoresHallados, 10)
plt.ylabel('frequencia')
plt.xlabel('fichas')
plt.title('Histograma')
plt.show()