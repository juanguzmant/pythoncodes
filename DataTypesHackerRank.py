# -*- coding: utf-8 -*-
"""
Created on Wed May 16 17:31:54 2018

@author: Juan
"""

i = 4
d = 4.0
s = 'HackerRank '

# Declare second integer, double, and String variables.
i2 = 0
d2 = 0
s2 = ''
# Read and save an integer, double, and String to your variables.
i2 = int(input())
d2 = float(input())
s2 = str(input())
# Print the sum of both integer variables on a new line.
print(sum(i+i2))
print(sum(d+d2))
print(s+s2)
# Print the sum of the double variables on a new line.

# Concatenate and print the String variables on a new line
# The 's' variable above should be printed first.