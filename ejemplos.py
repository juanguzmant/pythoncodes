# -*- coding: utf-8 -*-
"""
Created on Mon May  7 17:33:08 2018

@author: juan Camilo Guzman
"""

from clases import sistema, masa, resorte, restriccion, plano, restriccionPlano, esfera, rampaParabolica, esferaIrregular
import matplotlib.pyplot as plt
import numpy as np

plt.close('all') 
caso=7


if(caso==7):
    vectorRestriccion = []
    sist=sistema(xlim=[-50,50], ylim=[-50,50], dt=0.001, g=np.array([0,0]), paso=50)
    sist.ro=np.array([0,0])
    sist.Kg=5000
    esferaIrregular(tipo=esfera.resorte, vo=np.array([15,0]), ro=np.array([0,20]), Kv=0.1, b=5, n=100, Kmax=2)
    esferaIrregular(tipo=esfera.resorte, vo=np.array([-15,0]), ro=np.array([0,-20]), Kv=0.1, b=5, n=100, Kmax=2)
    esferaIrregular(tipo = esfera.resorte, vo = np.array([16,15]),ro = np.array([-15, 10]), Kv = 0.1, b = 5, n = 100, Kmax = 2)
    '''for i in range(len(sist.masas) -1):
        for j in range(i+1, len(sist.masas)):
           restriccion(m1 = sist.masas[i], m2 = sist.masas[j], tipo=restriccion.dFija, do = 0.5)   '''        
    for ma in sist.masas:
        ma.gravitacional=1
    for re in sist.resortes:
        re.Kmax=1.5
    plt.grid()
    sist.correr(5)
    


    
    