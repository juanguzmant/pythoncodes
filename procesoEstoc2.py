# -*- coding: utf-8 -*-
"""
Created on Thu May 17 19:48:08 2018

@author: Juan
"""

import numpy as np
from matplotlib.pyplot import hist
import random
n= 100
a = np.arange(1,n + 1)
print(a)

proceso = []
variableAleatoria = []
for i in a:
    aleatorio = random.randint(1,10)
    variableAleatoria.append(aleatorio)
    
for i in range(len(a)):
    if variableAleatoria[i] >= 2:
        if variableAleatoria[i]%2 == 0 and variableAleatoria[i-1]%2 ==0:
         proceso.append(variableAleatoria[i]**2)
        else:
         proceso.append(variableAleatoria[i])
    else:
         proceso.append(variableAleatoria[i])
print(variableAleatoria)
print(proceso)
hist(proceso, bins = 20)