# -*- coding: utf-8 -*-
"""
Created on Thu May  3 11:29:57 2018

@author: Juan
"""

def is_leap(year):
    leap = False
    
    if year%4 == 0:
        if year%100 ==0:
            leap = False
        if year%400 == 0:
            leap = True
        leap = True

    
    return leap

year = int(input())
print(is_leap(year))