diccionario = {} #diccionario vacio
diccionario2 = {'l':20,'m':21,'n':22}
abecedario = ('a','b','c','d','e','f','g','h','i','j','k') #tupla

for x in range(0,10):
	diccionario[abecedario[x]] = x+1 #agragando valores dinamicamente al dicc
#print(diccionario.get('z',-1)) # se obtiene valor del dicc, sino se retorna un valor que se quiera
#del diccionario['c'] #borrar elemento por su llave
llaves = list(diccionario.keys()) # objeto iterable, list() para convertirlo a lista pura
valores  =  list(diccionario.values())
print(llaves)
print(valores)
diccionario.update(diccionario2) #extendiente el diccionario a partir de otro
print(diccionario)