# -*- coding: utf-8 -*-
"""
Created on Thu May 17 09:31:43 2018

@author: Juan
"""

from clases import sistema, masa, resorte, restriccion, plano, restriccionPlano, esfera
import matplotlib.pyplot as plt
import numpy as np

plt.close('all') 
sist=sistema(xlim=[-30,30], ylim=[-30,30], dt=0.001, g=np.array([0,0]), paso=10)
sist.ro=np.array([0,0])
sist.Kg=5000
esfera(tipo=esfera.resorte, vg=3, vo=np.array([15,0]), ro=np.array([0,20]), Kv=0.1, b=5, n = 20)
esfera(tipo=esfera.resorte, vg=3, vo=np.array([-15,0]), ro=np.array([0,-20]), Kv=0.1, b=5)

for ma in sist.masas:
    ma.gravitacional=1
for re in sist.resortes:
    re.Kmax=1.5
plt.grid()
sist.correr(5)