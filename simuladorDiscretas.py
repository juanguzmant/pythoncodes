# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 18:49:15 2018

@author: Juan
"""
#importando modulos necesarios
#%matplotlib inline

import matplotlib.pyplot as plt
import numpy as np 
from scipy import stats 
import seaborn as sns 

#np.random.seed(2016) # replicar random
# parametros esteticos de seaborn
sns.set_palette("colorblind", desat=.6)

sns.set_context(rc={"figure.figsize": (10, 4)})

# Graficando histograma
mu, sigma = 0, 0.2 # media y desvio estandar
datos = np.random.normal(mu, sigma, 1000) #creando muestra de datos

# histograma de distribución normal.
plt.hist(datos, 20)
plt.ylabel('frequencia')
plt.xlabel('valores')
plt.title('Distribucion Normal')
plt.show()
'''
# Graficando FMP
n, p = 30, 0.4 # parametros de forma de la distribución binomial
#n_1, p_1 = 20, 0.3 # parametros de forma de la distribución binomial
x = np.arange(stats.binom.ppf(0.01, n, p),
              stats.binom.ppf(0.99, n, p))
print(x)
#x_1 = np.arange(stats.binom.ppf(0.01, n_1, p_1),
              #stats.binom.ppf(0.99, n_1, p_1))
fmp = stats.binom.pmf(x, n, p) # Función de Masa de Probabilidad
#fmp_1 = stats.binom.pmf(x_1, n_1, p_1) # Función de Masa de Probabilidad
plt.plot(x, fmp, '--')
#plt.plot(x_1, fmp_1)
plt.vlines(x, 0, fmp, colors='b', lw=5, alpha=0.5)
#plt.vlines(x_1, 0, fmp_1, colors='g', lw=5, alpha=0.5)
plt.title('Función de Masa de Probabilidad')
plt.ylabel('probabilidad')
plt.xlabel('valores')
plt.show()'''