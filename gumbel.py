# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 00:07:53 2018

@author: Juan
"""

from scipy.stats import gumbel_r
import matplotlib.pyplot as plt
import numpy as np
fig, ax = plt.subplots(1, 1)

x = np.linspace(gumbel_r.ppf(0.01),
gumbel_r.cdf(0.99), 100)
ax.plot(x, gumbel_r.pdf(x),'r--', lw=5, alpha=0.5, label='gumbel_r pdf')

'''
r = gumbel_r.rvs(size=1000)
ax.hist(r, normed=True, histtype='bar', alpha=0.8)
ax.legend(loc='best', frameon=False)
plt.show()'''
