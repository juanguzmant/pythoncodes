# -*- coding: utf-8 -*-
"""
Created on Wed Apr  4 06:54:00 2018

@author: Roberto Hincapie
"""
import numpy as np
import matplotlib.pyplot as plt

class resorte():
    sist=0
    normal=1
    tension=2
    compresion=3
    activo=4
    inactivo=5
    destruido=0
    bien=1
    
    def __init__(self, Kr=200, Lo=1, b=5.5, m1=0, m2=0, tipo=normal, Kmax=10000):
        self.Kr=Kr
        self.Lo=Lo
        self.m1=m1
        self.m2=m2
        self.b=b
        self.tipo=tipo
        self.sist.resortes.append(self)
        tmp,=plt.plot([self.m1.r[0], self.m2.r[0]],[self.m1.r[1], self.m2.r[1]],'-k')
        self.sist.lineas.append(tmp)
        self.estado=self.activo
        self.condicion=self.bien
        self.Kmax=Kmax
        
    def calcularFuerza(self):
        A=self.m2.r-self.m1.r
        d=np.linalg.norm(A)
        if(d>self.Kmax*self.Lo):
            self.condicion=self.destruido
            
        u=A/d
        self.estado=self.inactivo
        F=self.Kr*(d-self.Lo)*u+self.b*np.dot(self.m2.v-self.m1.v,u)*u
        if(self.tipo==self.normal or (self.tipo==self.tension and d>=self.Lo) or (self.tipo==self.compresion and d<=self.Lo)):
            if(self.condicion==self.bien):
                self.estado=self.activo
                self.m1.F=self.m1.F+F
                self.m2.F=self.m2.F-F
        else:
            self.estado=self.inactivo

class restriccion:
    dMin=0
    dFija=1
    sist=0
    Beta=0.1
    def __init__(self, m1=0, m2=0, e=0.8, tipo=dMin, do=1):
        self.m1=m1
        self.m2=m2
        if(tipo==self.dMin):
            self.do=m1.R+m2.R
        else:
            self.do=do
        self.e=e
        self.tipo=tipo
        #self.do=do
        self.sist.restricciones.append(self)
        tmp,=plt.plot([self.m1.r[0], self.m2.r[0]],[self.m1.r[1], self.m2.r[1]],'-k')
        self.sist.lineasRest.append(tmp)
        
        
    def AABB(self):   #Metodo que permite conocer si dos cuerpos están por lo menos cerca a colisionar
        return ~(   (self.m1.r[0]+self.m1.R<self.m2.r[0]-self.m2.R) or 
                 (self.m1.r[0]-self.m1.R>self.m2.r[0]+self.m2.R) or
                 (self.m1.r[1]+self.m1.R<self.m2.r[1]-self.m2.R) or 
                 (self.m1.r[1]-self.m1.R>self.m2.r[1]+self.m2.R) )
    
    def corregir(self):
        r1=self.m1.r
        r2=self.m2.r
        v1=self.m1.v
        v2=self.m2.v
        dist=np.linalg.norm(r1-r2)
        J=np.append(r1-r2, r2-r1)
        V=np.append(v1, v2)
        M=np.append(np.array([1.0, 1.0])/self.m1.m, np.array([1.0, 1.0])/self.m2.m)
        C=(dist**2-self.do**2)/2
        alpha=np.dot(np.multiply(J,J),M)
        aplicar=0
        
        if(self.tipo==self.dFija):
            V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V)+C*restriccion.Beta/self.sist.dt)/alpha
            aplicar=1
        if(self.tipo==self.dMin):
            vPerp=np.dot(v2-v1,r2-r1)/dist
            if((dist<=self.do and (vPerp<-0.1))):
                V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V))/alpha
                aplicar=1
            if(abs(vPerp)<0.1):
                V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V)+C*restriccion.Beta/self.sist.dt)/alpha
                aplicar=1
        if(aplicar==1):
            if(self.m1.tipo==masa.movil):
                self.m1.v=V2[0:2]
            if(self.m2.tipo==masa.movil):
                self.m2.v=V2[2:4]
            
class restriccionPlano:
    sist=0
    
    def __init__(self, m1, do, plano,e=1):  #Clase que representa una restriccion de distancia constante
        self.m1=m1
        self.do=do
        self.plano=plano
        self.e=e
        self.sist.restricciones.append(self)
        
    def AABB(self):   #Metodo que permite conocer si dos cuerpos están por lo menos cerca a colisionar
        return ~(   (self.m1.r[0]+self.m1.R<self.plano.xmin) or 
                 (self.m1.r[0]-self.m1.R>self.plano.xmax) or
                 (self.m1.r[1]+self.m1.R<self.plano.ymin) or 
                 (self.m1.r[1]-self.m1.R>self.plano.ymax) )

    def corregir(self):
        r1=self.m1.r
        v1=self.m1.v
        dp, r2 = self.plano.pertenece(r1)
        #print("Plano: ",self.plano.r0,", ",self.plano.r1, ". Punto: ",r1,", dp=",dp,"r2: ",r2)
        vPerp=np.dot(v1,(r1-r2)/dp)
        vtang=np.dot(v1,self.plano.u)
        d0=np.linalg.norm(self.m1.r-self.plano.r0)
        d1=np.linalg.norm(self.m1.r-self.plano.r1)
        
        #if(dp<=self.do):
        #    print("Metodo corregir del plano: ",self.plano.r0,", ",self.plano.r1, ". Punto: ",r1,", dp=",dp,"r2: ",r2)
        #    print("Vperp: ",vPerp)
        if(dp<=self.do and ((vPerp<0 and abs(vPerp)>0.1) or abs(vPerp)<0.1 )):
            #print("Vperp: ",vPerp, "W: ",self.plano.w)
            #print("Vtang: ",vtang)
            
            #print("v inicial: ",v1)
            #print("Metodo corregir del plano: ",self.plano.r0,", ",self.plano.r1, ". Punto: ",r1,", dp=",dp,"r2: ",r2)
            J=np.append(r1-r2, r2-r1)
            V=np.append(v1, 0*v1)
            M=np.append(np.array([1.0, 1.0])/self.m1.m, np.array([1.0, 1.0])/self.m1.m/1e9)
            C=(dp**2-self.do**2)/2
            #print("J=",J)
            #print("V=",V)
            #print("Mi",M)
            #print("C=",C)
            alpha=np.dot(np.multiply(J,J),M)
            #V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V)+C*restriccion.Beta/self.sist.dt)/alpha
            #V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V)+C*restriccion.Beta/self.sist.dt)/alpha
            if(abs(vPerp)<0.1):
                V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V)+C*restriccion.Beta/self.sist.dt)/alpha
            else:
                V2=V-(1+self.e)*np.multiply(J,M)*(np.dot(J,V))/alpha
            #print("V2=",V2)
            if(self.m1.tipo==masa.movil):
                self.m1.v=V2[0:2]
            #print("v final: ",self.m1.v,"\n")

class plano:
    def __init__(self, r0, r1):
        self.r0=r0
        self.r1=r1
        self.A=r1-r0
        self.d=np.linalg.norm(self.A)
        self.u=self.A/self.d
        self.w=np.array([-self.u[1], self.u[0]])
        plt.plot([r0[0], r1[0]],[r0[1], r1[1]],'-r')
        self.xmax=max(self.r0[0], self.r1[0])
        self.xmin=min(self.r0[0], self.r1[0])
        self.ymax=max(self.r0[1], self.r1[1])
        self.ymin=min(self.r0[1], self.r1[1])
        
    def proy(self,r):
        x1=np.dot(r-self.r0,self.u)
        y1=np.dot(r-self.r0,self.w)
        punto=self.r0+x1*self.u
        return x1, abs(y1), punto

    def pertenece(self,r):
        x,y,punto=self.proy(r)
        if(x>=0 and x<=self.d):
            if(y>0):
                return y, punto
        return 1e20,-1
        
class masa:     
    sist=0
    fijo=0
    movil=1
    
    ang=np.arange(0,2*np.pi+0.3,0.3)
    x=np.cos(ang)
    y=np.sin(ang)
    def __init__(self, m=1, r=np.array([0.0,0.0]), v=np.array([0.0,0.0]), Kv=0.5, tipo=movil, R=1, gravitacional=0):
        
        self.m=m
        self.r=r
        self.v=v
        self.Kv=Kv
        self.tipo=tipo
        self.F=np.array([0.0,0.0])
        self.sist.masas.append(self)
        self.R=R
        tmp,=plt.plot(self.r[0]+self.R*self.x,self.r[1]+self.R*self.y,'-b')
        self.sist.puntos.append(tmp)
        self.gravitacional=gravitacional
        
    
        
    def calcularFuerza(self):
        if(self.gravitacional==0):
            self.F=self.m*self.sist.g-self.Kv*self.v 
        else:
            A=self.sist.ro-self.r
            d=np.linalg.norm(A)
            u=A/d
            self.F=self.sist.Kg*u/d**2-self.Kv*self.v 
    
    def calcularEuler(self):
        self.calcularPosicion()
        self.calcularVelocidad()
    
    def calcularVelocidad(self):
        if(self.tipo==self.movil):
            self.v=self.v+self.sist.dt*self.F/self.m
    def calcularPosicion(self):
        if(self.tipo==self.movil):
            self.r=self.r+self.sist.dt*self.v
    def setR(self, t):
        pass
    
    
    
    
    
    
    
    
class sistema:
    def __init__(self, dt=0.01, g=np.array([0.0,-9.8]), xlim=[-1,1], ylim=[-1,1], paso=1):
        self.dt=dt
        self.t=0
        self.g=g
        masa.sist=self
        resorte.sist=self
        restriccion.sist=self
        restriccionPlano.sist=self
        
        self.masas=[]
        self.resortes=[]
        self.restricciones=[]
        self.puntos=[]
        self.lineas=[]
        self.lineasRest=[]
        self.fig=plt.figure()
        self.ax=plt.subplot(111)
        #plt.axis('equal')
        self.ax.set_xlim(xlim)
        self.ax.set_ylim(ylim)
        self.paso=paso
        self.contador=paso
        

    def correr(self, tmax):
        self.t=0
        while(self.t<=tmax):
            #self.masas[0].r=np.array([0, 0.1*np.sin(2*np.pi/1*self.t+np.pi/2)])
            #Cálculo de las fuerzas de friccion y pesos
            for i in range(len(self.masas)):
                if np.linalg.norm(self.masas[i].r) < 5:
                    self.masas[i].r = np.array([200, 220])
                    self.masas[i].v = np.array([0.0, 0.0])
                    #self.masas[i].tipo = self.masa.fija
                else:
                    self.masas[i].calcularFuerza()
                    self.masas[i].setR(self.t)
                
            #Cálculo de las fuerzas de resortes
            for i in range(len(self.resortes)):
                self.resortes[i].calcularFuerza()
            #Cálculo de las nuevas velocidades exclusivamente
            for i in range(len(self.masas)):
                self.masas[i].calcularVelocidad()
            #Procedimiento de análisis de las restricciones
            #Revisión de las posibles colisiones
            porAnalizar=[]
            for re in self.restricciones:
                if(isinstance(re,restriccionPlano)):
                    if(re.AABB()):
                        porAnalizar.append(re)
                if(isinstance(re,restriccion)):
                    if(re.tipo==restriccion.dFija or (re.tipo==restriccion.dMin and re.AABB())):
                        porAnalizar.append(re)
            #print(len(porAnalizar))
            #Proceso de las colisiones, se realiza hasta que no quede ninguna colisión por resolver
            for j in range(5):
                for re in porAnalizar:
                    re.corregir()
            
            for i in range(len(self.masas)):
                self.masas[i].calcularPosicion()
            
            self.t=self.t+self.dt
            self.contador-=1
            if(self.contador<=0):
                self.graficar()
                self.contador=self.paso
            #Ensayo de la expansion

            
    def graficar(self):
        for i in range(len(self.masas)):
            self.puntos[i].set_xdata(self.masas[i].r[0]+self.masas[i].R*masa.x)
            self.puntos[i].set_ydata(self.masas[i].r[1]+self.masas[i].R*masa.y)
        for i in range(len(self.resortes)):
            if(self.resortes[i].estado==resorte.activo):
                self.lineas[i].set_xdata([self.resortes[i].m1.r[0],self.resortes[i].m2.r[0]] )
                self.lineas[i].set_ydata([self.resortes[i].m1.r[1],self.resortes[i].m2.r[1]] )
            else:
                self.lineas[i].set_xdata([self.resortes[i].m1.r[0]] )
                self.lineas[i].set_ydata([self.resortes[i].m1.r[1]] )
        for i in range(len(self.restricciones)):
            if(isinstance(self.restricciones[i], restriccion)):
                if(self.restricciones[i].tipo==restriccion.dFija):
                    self.lineasRest[i].set_xdata([self.restricciones[i].m1.r[0],self.restricciones[i].m2.r[0]] )
                    self.lineasRest[i].set_ydata([self.restricciones[i].m1.r[1],self.restricciones[i].m2.r[1]] )
                else:
                    self.lineasRest[i].set_xdata([self.restricciones[i].m1.r[0]] )
                    self.lineasRest[i].set_ydata([self.restricciones[i].m1.r[1]] )
        cad="t="+str(self.t)
        plt.title(cad)
        self.fig.canvas.draw()
        plt.pause(0.002)


class esfera:
    restriccion=0
    resorte=1
    def __init__(self, R=1, ro=np.array([0,0]), vo=np.array([0,0]), vg=0, m=1, tipo=restriccion, Kr=100, b=0.5, n=10, Kv=0):
        m1=masa(m=m, r=ro, v=vo, Kv=Kv, R=0.1)
        ma=[]
        for theta in np.arange(0, np.pi*2, np.pi*2/n):
            r=ro+R*np.array([np.cos(theta), np.sin(theta)])
            v=vo+vg*np.array([np.cos(np.pi/2+theta), np.sin(np.pi/2+theta)])
            ma.append(masa(m=m, r=r, v=v, Kv=Kv, R=0.1))
            if(tipo==self.resorte):
                re=resorte(m1=m1, m2=ma[-1], Kr=Kr, Lo=R, b=b)
                if(len(ma)>1):
                    re=resorte(m1=ma[-2], m2=ma[-1], Kr=Kr, Lo=np.linalg.norm(ma[-1].r-ma[-2].r), b=b)
        re=resorte(m1=ma[0], m2=ma[-1], Kr=Kr, Lo=np.linalg.norm(ma[-1].r-ma[0].r), b=b)
                    
class rampaParabolica:
    def __init__(self, H=1, L=1, n=10, ro=np.array([0,0]), e=1):
        A=4*H/L**2
        dx=L/n
        p=[]
        self.e=e
        for x in np.arange(-L/2, L/2, dx):
            y=A*x**2
            ra=ro+np.array([x,y])
            rb=ro+np.array([x+dx,A*(x+dx)**2])
            p.append(plano(r0=ra, r1=rb))
        self.p=p
    def restringir(self, m=0):
        for pl in self.p:
            re=restriccionPlano(m1=m, plano=pl, do=m.R, e=self.e)
    
class esferaIrregular:
    restriccion=0
    resorte=1
    K=10
    def __init__(self, R=3, ro=np.array([0,0]), vo=np.array([0,0]), M=1, tipo=resorte, Kr=100, b=0.5, n=20, Kv=0, Kmax=1000):
        ma=[]
        iter=0
        while(len(ma)<n and iter<100000):
            iter+=1
            if(iter<50000):
                R=R*2
            r=ro+R*np.array([np.random.rand()*2*R-R, np.random.rand()*2*R-R])
            if(iter<50000):
                R=R/2
            if(np.linalg.norm(r-ro)>R):
                u=(r-ro)/np.linalg.norm(r-ro)
                r=ro+u*R
            dmin=100000
            for m in ma:
                dmin=min(dmin, np.linalg.norm(r-m.r))
            if(dmin>2*R/self.K):
                ma.append(masa(m=M,r=r, v=vo, Kv=Kv, R=R/self.K/4))
        #PRoceder a conectar
        r1=R/np.sqrt(len(ma))*2.5
        for i in range(len(ma)-1):
            for j in range(i+1, len(ma)):
                d=np.linalg.norm(ma[i].r-ma[j].r)
                if(d<=r1):
                    re=resorte(Kr=Kr, b=b, Lo=d, m1=ma[i], m2=ma[j], Kmax=Kmax+np.random.rand())
            #if(tipo==self.resorte):
            #    re=resorte(m1=m1, m2=ma[-1], Kr=Kr, Lo=R, b=b)
            #    if(len(ma)>1):
            #        re=resorte(m1=ma[-2], m2=ma[-1], Kr=Kr, Lo=np.linalg.norm(ma[-1].r-ma[-2].r), b=b)
        #re=resorte(m1=ma[0], m2=ma[-1], Kr=Kr, Lo=np.linalg.norm(ma[-1].r-ma[0].r), b=b)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    