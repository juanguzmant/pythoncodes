# -*- coding: utf-8 -*-
"""
Created on Thu May 17 12:43:09 2018

@author: Juan
"""

import numpy as np
from matplotlib.pyplot import hist
import random
n= 100
a = np.arange(1,n + 1)
print(a)

proceso = []
variableAleatoria = []
for i in a:
    aleatorio = random.randint(1,10)
    variableAleatoria.append(aleatorio)
for i in range(len(a)):
     if(len(variableAleatoria) >= 3):
         temp = variableAleatoria[i] + variableAleatoria[i-1] + variableAleatoria[i-2]
         proceso.append(round(temp/3,3))
     else:
         proceso.append(variableAleatoria[i])
print(variableAleatoria)
print(proceso)
hist(proceso, bins = 20)

