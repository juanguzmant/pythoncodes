# -*- coding: utf-8 -*-
#Juan Camilo Guzman Toro
#318957
''' Numero de fichas = 25
	10 grises 	(1)
	5 rojas	(5)
	4 verdes	(25)
	4 azules	(50)
	2 negras	(100) '''
import random
import matplotlib.pylab as plt
import statistics as stats
import numpy as np
import scipy.stats as ss
 
valores = []


numeroLanzamientos = int(input('Numero de lanzamientos: '))


for i in range(numeroLanzamientos):
    numero =  random.random()
    if numero <= 0.43:
        valores.append(1)
    elif numero <= 0.61:
           valores.append(5)
    elif numero <= 0.86:
        valores.append(25)
    elif numero <= 0.96:
        valores.append(50)
    elif numero <= 1:
        valores.append(100)
    
#print(valores,"\n")
g = valores.count(1)
r = valores.count(5)
v = valores.count(25)
a = valores.count(50)
n = valores.count(100)


#print("{}\n{}\n{}\n{}\n".format(g,r,v,a,n))
plt.hist(valores, bins = 50)